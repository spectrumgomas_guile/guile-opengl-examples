(use-modules (gl) (glut) (gl low-level))

(define (init)
    (gl-ortho 0.0 100.0 0.0 100.0 -1.0 1.0)
    (set-gl-clear-color 1.0 1.0 1.0 0.0))

(define (draw-scene)
    (gl-clear (clear-buffer-mask color-buffer))
    (gl-color 0 0 0)
    
    (glPolygonMode (cull-face-mode front) (mesh-mode-2 line))
    (glPolygonMode (cull-face-mode back) (mesh-mode-2 fill))

    (gl-begin (begin-mode triangles)
        ;; CCW
        (gl-vertex 20.0  80.0  0.0)
        (gl-vertex 20.0  20.0  0.0)
        (gl-vertex 50.0  80.0  0.0)

        ;; CCW
        (gl-vertex 50.0  80.0  0.0)
        (gl-vertex 20.0  20.0  0.0)
        (gl-vertex 50.0  20.0  0.0)       

        ;; CW
        (gl-vertex 50.0  20.0  0.0)
        (gl-vertex 50.0  80.0  0.0)
        (gl-vertex 80.0  80.0  0.0)

        ;; CCW
        (gl-vertex 80.0  80.0  0.0)
        (gl-vertex 50.0  20.0  0.0)
        (gl-vertex 80.0  20.0  0.0)))


(define (on-display)
    (init)
    (draw-scene)
    (swap-buffers))

(initialize-glut #:window-size '(800 . 800))
(make-window "back-faces-filled")
(set-display-callback (lambda() (on-display)))
(glut-main-loop)